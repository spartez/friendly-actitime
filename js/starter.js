$(() => {
    Backbone.Marionette.Renderer.render = (template, data) => {
        return Mustache.to_html(template, data);
    };

    let optionsModel = new OptionsModel();
    let balanceModel = new BalanceModel();
    let workTimeModel = new WorkTimeModel();

    // Update start time
    new StartTimeController({
        workTimeModel : workTimeModel
    });

    // Initialize balance
    new BalanceController({
        model : balanceModel,
        optionsModel: optionsModel
    });

    // Show nagger is needed
    new NaggerView({
        balanceModel: balanceModel,
        optionsModel: optionsModel
    });
});