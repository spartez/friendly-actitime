class WorkTimeModel extends Backbone.Model {
    initialize() {
        this._loadFromStore();
    }
    defaults() {
        return {
            startTime: undefined,
            stopTime: undefined
        }
    }
    save(callback) {
        var start = this.getStartTime();
        chrome.storage.sync.set({
            factitime: start ? start.getTime() : undefined
        }, callback ? callback : function () {
        });
    }
    getStartTime() {
        return this.get('startTime');
    }
    setStartTime(value) {
        this.set('startTime', value);
    }
    getStopTime() {
        return this.get('stopTime');
    }
    setStopTime(value) {
        this.set('stopTime', value);
    }
    _loadFromStore() {
        chrome.storage.sync.get('factitime', (store) => {
            let startDate;
            if (store && store.factitime) {
                let startTime = parseInt(store.factitime);
                startDate = new Date(startTime);
            } else {
                startDate = new Date();
                startDate.setHours(9);
                startDate.setMinutes(0);
                startDate.setSeconds(0);
            }
            this.setStartTime(startDate);
        });
    }
}