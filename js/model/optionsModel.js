class OptionsModel extends Backbone.Model {
    initialize() {
        this._loadFromStore();
    }
    save(callback) {
        chrome.storage.sync.set({
            startDate: this.getStartDate(),
            startBalance: this.getStartBalance(),
            serverUrl: this.getServerUrl()
        }, callback);
    }
    _loadFromStore() {
        chrome.storage.sync.get({
            startDate: TimeUtils.firstJanuaryThisYear(),
            startBalance: 0,
            serverUrl: 'https://'
        }, (store) => this.set(store));
    }
    getServerUrl() {
        return this.get('serverUrl');
    }
    getStartBalance() {
        return this.get('startBalance');
    }
    getStartDate() {
        return this.get('startDate');
    }
}
