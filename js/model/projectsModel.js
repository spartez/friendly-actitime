class ProjectsModel extends Backbone.Model {
    defaults() {
        return {
            projects: [],
            lastProject: 0
        }
    }
    initializeProjectsFromActitimeServer($form) {
        let taskColumns = $('.actualRow', $form);
        let mostReportedYesterdayProject = null;
        let projects = _.map(taskColumns, (taskColumn) => {
            let id = $('.taskNameColumn', taskColumn).attr('id').split("Cell")[1];
            let taskName = $('.taskNameColumn .task', taskColumn).text().trim();
            let projectName = $('.project', taskColumn).text().trim();
            let projectTaskName = taskName + " (" + projectName + ")";
            let dayNumber = new Date().getDay() - 2;
            let inputYesterday = $('input.inputTT', taskColumn)[dayNumber];
            let currentValue = inputYesterday && inputYesterday.value ? inputYesterday.value : "0:00";
            if (!mostReportedYesterdayProject || mostReportedYesterdayProject.value < currentValue) {
                mostReportedYesterdayProject = {id: id, value: currentValue};
            }
            return {value: id, text: projectTaskName};
        });
        let lastProjectId = mostReportedYesterdayProject ? mostReportedYesterdayProject.id : undefined;
        this.set({
            projects: projects,
            lastProject: lastProjectId
        });
    }
    getProjects() {
        return this.get('projects');
    }
}
