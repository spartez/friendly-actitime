class BalanceModel extends Backbone.Model {
    defaults() {
        return {
            balance: undefined,
            today: undefined,
            userName: 'Anonymous'
        }
    }
    load() {
        chrome.storage.local.get({
            balanceModel: this.defaults
        }, (store) => this.set(store.balanceModel));
    }
    save() {
        chrome.storage.local.set({
            balanceModel: this.toJSON()
        });
    }
    getBalance() {
        return this.get('balance');
    }
    getToday() {
        return this.get('today');
    }
    getUserName() {
        return this.get('userName');
    }
}
