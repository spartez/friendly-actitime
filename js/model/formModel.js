class FormModel extends Backbone.Model {
    initializeFormFromActitimeServer(serverUrl) {
        $.ajax({
            url: serverUrl + "/user/submit_tt.do?dateStr=" + TimeUtils.currentDateForUrl(),
            dataType: 'html'
        }).done((data) => {
            let cleanData = data.replace(/<img\s[^>]*?src\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/g, "");
            let form = $("form[name='SubmitTTForm']", cleanData);
            form.attr('action', serverUrl + '/user/submit_tt.do');
            $('script', form).detach();
            this.set('form', $(form[0]));
        });
    }
    getForm() {
        return this.get('form');
    }
}
