$(() => {
    Backbone.Marionette.Renderer.render = (template, data) => {
        return Mustache.to_html($(template).html(), data);
    };
    let App = new Backbone.Marionette.Application();

    App.addRegions({
        content: "#options-content"
    });
    var optionsModel = new OptionsModel();
    App.addInitializer(() => {
        let optionsView = new OptionsView({
            model: optionsModel
        });
        App.content.show(optionsView);
    });
    App.start();
});