class TimeUtils {
    static parseStringTimeToFloatHours(hoursString) {
        if (!hoursString) return 0;
        let times = hoursString.split(':');
        let h = parseInt(times[0]);
        let m = parseInt(times[1]);
        return h + m / 60;
    }
    static formattedDate(date) {
        let dd = date.getDate();
        let mm = date.getMonth() + 1; //January is 0!
        let yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        return yyyy + '-' + mm + '-' + dd;
    }
    static formattedTime(date) {
        if (isFinite(date)) {
            let hours = TimeUtils._as2DigitString(date.getHours());
            let minutes = TimeUtils._as2DigitString(date.getMinutes());
            return hours + ':' + minutes;
        } else {
            return "00:00";
        }
    }
    static formattedInterval(time) {
        let _23hours = TimeUtils._hoursToMilis(23); // because first is 1:00 not 0:00
        time = time + _23hours;
        return TimeUtils.formattedTime(new Date(time));
    }
    static formattedIntervalHours(time) {
        let timeInMilis = TimeUtils._hoursToMilis(time);
        return TimeUtils.formattedInterval(timeInMilis);
    }
    static parseStringTimeToDate(dateString) {
        let newDateStr = dateString.split(":");
        let newHours = parseInt(newDateStr[0]);
        let newMinutes = parseInt(newDateStr[1]);
        let newStartDate = new Date();
        newStartDate.setHours(newHours);
        newStartDate.setMinutes(newMinutes);
        return newStartDate;
    }
    static firstJanuaryThisYear() {
        let firstJanuary = new Date();
        firstJanuary.setMonth(0, 1);
        return firstJanuary.getTime();
    }
    static addFormattedIntervals(interval1, interval2) {
        let i1 = TimeUtils.parseStringTimeToFloatHours(interval1);
        let i2 = TimeUtils.parseStringTimeToFloatHours(interval2);
        let sum = i1 + i2;
        return TimeUtils.formattedIntervalHours(sum);
    }
    static currentDateForUrl() {
        let now = new Date();
        return now.getUTCFullYear() + TimeUtils._as2DigitString(now.getMonth()+1) + TimeUtils._as2DigitString(now.getUTCDate());
    }
    static _as2DigitString(number) {
        return number > 9 ? number.toString() : '0' + number;
    }
    static _hoursToMilis(hours) {
        return hours * 60 * 60 * 1000;
    }
}