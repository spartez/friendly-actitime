NavbarView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-navbar-tpl",
    initialize: function initialize(options) {
        this.options.optionsModel.on('change', this.render, this);
        this.options.balanceModel.on('change', this.render, this);
    },
    modelEvents: {
        "change": "render"
    },
    serializeData: function serializeData() {
        return {
            userName: PopupApp.model.balance.getUserName(),
            actitimeUrl: PopupApp.model.options.getServerUrl()
        };
    }
});