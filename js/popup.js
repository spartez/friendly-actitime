$(() => {
    Backbone.Marionette.Renderer.render = (template, data) => {
        return Mustache.to_html($(template).html(), data);
    };
    window.PopupApp = new Backbone.Marionette.Application();
    PopupApp.addRegions({
        navbar: "#factitime-navbar",
        messageLoggedOut: "#message-logged-out",
        messageBadServerUrl: "#message-bad-server-url",
        balance: "#factitime-balance",
        projects: "#projects",
        startTime: "#start-time",
        stopTime: "#stop-time",
        loggedWork: "#factitime-work-time",
        logWorkButton: "#log-work-time"
    });
    PopupApp.addInitializer(() => {
        PopupApp.model = {};
        PopupApp.model.options = new OptionsModel();
        PopupApp.model.balance = new BalanceModel();
        PopupApp.model.projects = new ProjectsModel();
        PopupApp.model.workTime = new WorkTimeModel();
        PopupApp.model.form = new FormModel();

        let navbarView = new NavbarView({
            optionsModel: PopupApp.model.options,
            balanceModel: PopupApp.model.balance
        });

        let fillOptionsMessageView = new FillOptionsMessageView({
            optionsModel: PopupApp.model.options
        });

        let loggedOutMessageView = new LoggedOutMessageView({
            model: PopupApp.model.balance,
            optionsModel: PopupApp.model.options
        });

        let balanceView = new BalanceView({
            model: PopupApp.model.balance
        });

        let projectsView = new ProjectsView({
            model: PopupApp.model.projects
        });

        let startTimeView = new StartTimeView({
            model: PopupApp.model.workTime
        });

        let stopTimeView = new StopTimeView({
            model: PopupApp.model.workTime
        });

        let loggedWorkView = new LoggedWorkView({
            model: PopupApp.model.workTime,
            balanceModel: PopupApp.model.balance
        });

        let logWorkButtonView = new LogWorkButtonView({
            formModel: PopupApp.model.form,
            optionsModel: PopupApp.model.options,
            workTimeModel: PopupApp.model.workTime
        });

        PopupApp.controller = {};

        new FormController({
            model: PopupApp.model.form,
            optionsModel: PopupApp.model.options,
            view: projectsView
        });

        new ProjectsController({
            model: PopupApp.model.projects,
            formModel: PopupApp.model.form,
            view: projectsView
        });

        PopupApp.controller.balanceController = new BalanceController({
            model : PopupApp.model.balance,
            optionsModel: PopupApp.model.options,
            isPopup: true
        });

        PopupApp.navbar.show(navbarView);
        PopupApp.messageBadServerUrl.show(fillOptionsMessageView);
        PopupApp.messageLoggedOut.show(loggedOutMessageView);
        PopupApp.balance.show(balanceView);
        PopupApp.projects.show(projectsView);
        PopupApp.startTime.show(startTimeView);
        PopupApp.stopTime.show(stopTimeView);
        PopupApp.loggedWork.show(loggedWorkView);
        PopupApp.logWorkButton.show(logWorkButtonView);
    });
    PopupApp.start();

    AnalyticsController.send('Popup', 'Show');

    function openLinksInNewTab() {
        $('body').on('click', 'a', (e) => {
            chrome.tabs.create({url: $(e.currentTarget).attr('href')});
            return false;
        });
    }

    openLinksInNewTab();
});