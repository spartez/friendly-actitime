StopTimeView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-stop-time-template",
    modelEvents: {
        "change:startTime": "refresh",
        "change:stopTime": "render"
    },
    ui: {
        stop: "#stop"
    },
    events: {
        "change @ui.stop": "stopTimeChanged"
    },
    initialize: function initialize() {
        this.stopTimes = [];
        this.selectedIndex = undefined;
    },
    serializeData: function serializeData() {
        return {stopTimes: this.stopTimes};
    },
    onRender: function onRender() {
        var selectedTime = this.ui.stop.find('option').get(this.selectedIndex);
        if (selectedTime) {
            $(selectedTime).attr('selected', 'selected');
        }
    },
    refresh: function refresh() {
        this.stopTimes = this.generateStopTimes();
        var stopTime = this.stopTimes[this.selectedIndex];
        if (stopTime) {
            this.updateStopTime(stopTime.value);
        }
    },
    stopTimeChanged: function stopTimeChanged() {
        var $selectedOption = this.ui.stop.find('option:selected');
        this.selectedIndex = this.ui.stop.find('option').index($selectedOption);
        var stopTime = $selectedOption.val();
        this.updateStopTime(stopTime);
        AnalyticsController.send('Popup', 'Change', 'Stop time');
    },
    updateStopTime: function updateStopTime(stopTime) {
        var stopDate = new Date(parseInt(stopTime));
        this.model.setStopTime(stopDate);
    },
    generateStopTimes: function generateStopTimes() {
        var currDate = this.model.getStartTime();
        if (currDate) {
            var nowDate = new Date(new Date().getTime() + 15 * 60 * 1000);
            var reasonableMaxDate = new Date();
            reasonableMaxDate.setHours(19);
            reasonableMaxDate.setMinutes(0);
            var endDate = new Date(Math.max(reasonableMaxDate, nowDate));
            var added = 0;
            var stopTimes = [];
            var selectedIndex = 0;
            while (added < 4 || currDate < endDate) {
                var selected = this.isTimeSelected(added, currDate);
                selectedIndex = selected ? added : selectedIndex;
                added++;
                stopTimes.push({value: currDate.getTime(), text: TimeUtils.formattedTime(currDate), selected: selected});
                currDate = new Date(currDate.getTime() + 15 * 60 * 1000);
            }
            var firstGeneration = !this.selectedIndex;
            if (firstGeneration) {
                this.selectedIndex = selectedIndex;
            }
            return stopTimes;
        } else {
            return [];
        }
    },
    isTimeSelected: function (index, time) {
        if (this.selectedIndex) {
            return index == this.selectedIndex;
        } else {
            var halfOf15mins = (7.5 * 60 * 1000);
            return Math.abs(time.getTime() - new Date().getTime()) < halfOf15mins;
        }
    }
});