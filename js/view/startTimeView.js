StartTimeView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-start-time-template",
    modelEvents: {
        "change": "modelChanged"
    },
    ui: {
        start: "#start"
    },
    events: {
        "change @ui.start": "startTimeChanged"
    },
    serializeData: function serializeData() {
        var startTime = this.model.getStartTime();
        if (startTime) {
            var formattedStartTime = TimeUtils.formattedTime(startTime);
            return {startTime: formattedStartTime};
        } else {
            return {startTime: ''};
        }
    },
    modelChanged: function modelChanged(event) {
        if (event._previousAttributes.startTime == undefined) {
            this.render();
        }
    },
    startTimeChanged: function startTimeChanged() {
        var startTime = this.ui.start.val();
        if (startTime) {
            var startDate = this._stringDateToDate(startTime);
            this.model.setStartTime(startDate);
            this.model.save();
        }
        AnalyticsController.send('Popup', 'Change', 'Start time');
    },
    _stringDateToDate: function _stringDateToDate(dateString) {
        dateString = dateString || $(this).val();
        return TimeUtils.parseStringTimeToDate(dateString);
    }
});