LoggedOutMessageView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-message-warn",
    modelEvents: {
        "change": "modelChanged"
    },
    ui: {
        "alert": "div.alert"
    },
    initialize: function initialize(options) {
        this.optionsModel = options.optionsModel;
    },
    serializeData: function serializeData() {
        return {message: this._getMessage()};
    },
    modelChanged: function modelChanged() {
        if (_.isNaN(this.model.getBalance())) {
            this.render();
            this.ui.alert.show();
        }
    },
    _getMessage: function _getMessage() {
        var messageTemplate = "Log in to <a href='{{{serverUrl}}}'>actiTime</a> first!";
        return Mustache.to_html(messageTemplate, {serverUrl: this.optionsModel.getServerUrl()});
    }

});