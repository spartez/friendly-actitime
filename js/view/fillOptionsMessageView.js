FillOptionsMessageView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-message-warn",
    ui: {
        "alert": "div.alert"
    },
    initialize: function initialize(options) {
        this.optionsModel = options.optionsModel;
        this.listenTo(this.optionsModel, 'change', this.modelChanged);
        this.modelChanged();
    },
    serializeData: function serializeData() {
        return {message: this._getMessage()};
    },
    modelChanged: function modelChanged() {
        var serverUrl = this.optionsModel.getServerUrl();
        var isServerUrlUndefined = _.isUndefined(serverUrl);
        if (isServerUrlUndefined || serverUrl.match(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/g) === null) {
            this.render();
            this.ui.alert.show();
        }
    },
    _getMessage: function _getMessage() {
        var messageTemplate = "You have to set actitime server URL in plugin's <a href='{{{optionsUrl}}}'>options</a> first!";
        return Mustache.to_html(messageTemplate, {optionsUrl: chrome.extension.getURL("options.html") });
    }
});