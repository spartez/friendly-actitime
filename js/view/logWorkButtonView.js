LogWorkButtonView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-log-work",
    events: {
        "click @ui.logWorkBtn": "saveWorkTime"
    },
    ui: {
        logWorkBtn: ".log-work"
    },
    initialize: function initialize(options) {
        this.workTimeModel = options.workTimeModel;
        this.formModel = options.formModel;
        this.optionsModel = options.optionsModel;
    },
    saveWorkTime: function saveWorkTime(e) {
        e.preventDefault();
        var timeToLog = $('span.toBeDone').html();
        var selectedProjectId = $("#project-selector").val();
        this._logTime(selectedProjectId, timeToLog);
        var newStartTime = this._getFinishDataFromSelect();
        $('input[name=startTime]').val(TimeUtils.formattedTime(newStartTime));
        this._saveNewStartTime(newStartTime);
    },
    _getFinishDataFromSelect: function _getFinishDataFromSelect() { // todo duplicated in loggedWorkView
        var $select = $('select[name=stopTime]');
        return new Date(parseInt($select.val()));
    },
    _saveNewStartTime: function _saveNewStartTime(newStartDate) {
        this.workTimeModel.setStartTime(newStartDate);
        this.workTimeModel.save();
    },
    _logTime: function _logTime(projectId, timeToLog) {
        var $form = this.formModel.getForm();  // this shouldn't use $form
        $('#factitime-form').html($form);
        var d = new Date();
        var dayNumber = d.getDay() - 1;
        var $projectTodaysInput = $("#spent_" + projectId + "_" + dayNumber);
        var loggedTime = $projectTodaysInput.val();
        var sumaricValue = TimeUtils.addFormattedIntervals(loggedTime, timeToLog);
        $projectTodaysInput.val(sumaricValue);
        $("input[name='pageAction']").val('save_tt');
        var serverUrl = this.optionsModel.getServerUrl();
        AnalyticsController.send('Popup', 'Click', 'Log work', TimeUtils.parseStringTimeToFloatHours(timeToLog));
        $.post(serverUrl + '/user/submit_tt.do', $form.serialize()).done(function () {
            $("#message").show();
            PopupApp.controller.balanceController.refreshBalance();
        });
        console.log('changed spent time form added');
    }
});