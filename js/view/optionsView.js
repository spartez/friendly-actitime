OptionsView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-options-template",
    ui: {
        serverUrl: '#server-url',
        startDate: '#start-date',
        startBalance: '#start-balance',
        status: "#status",
        save: "#save"
    },
    modelEvents: {
        "change": "modelChanged"
    },
    events: {
        "click @ui.save": "saveOptions"
    },
    saveOptions: function saveOptions() {
        this.model.set({
            startDate: new Date(this.ui.startDate.val()).getTime(),
            startBalance: parseFloat(this.ui.startBalance.val()),
            serverUrl: this.ui.serverUrl.val()
        });
        var that = this;
        this.model.save(function () {
            var status = that.ui.status;
            status.text('Options saved.');
            setTimeout(function () {
                status.text('');
            }, 750);
        });
    },
    modelChanged: function modelChanged() {
        this.ui.startDate[0].valueAsDate = new Date(this.model.getStartDate());
        this.ui.startBalance.val(this.model.getStartBalance());
        this.ui.serverUrl.val(this.model.getServerUrl());
    }
});