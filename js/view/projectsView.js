ProjectsView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-projects-template",
    ui: {
        projectsSelect: '#project-selector'
    },
    modelEvents: {
        change: "render"
    },
    events: {
        'change @ui.projectsSelect': "projectChanged"
    },
    serializeData: function serializeData() {
        return {projects: this.model.getProjects()};
    },
    projectChanged: function() {
        AnalyticsController.send('Popup', 'Change', 'Project');
    },
    onRender: function onRender() {
        var lastProject = this.model.get('lastProject');
        if (lastProject) {
            this.ui.projectsSelect.val(lastProject);
        }
    }
});