NaggerView = Backbone.Marionette.ItemView.extend({
    template: '<div id="faktitime-nag-message" style="display:inline-block;">' +
        '<a id="actitime-link" href="{{{serverUrl}}}/user/submit_tt.do">' +
        '<img src="{{{naggerImgUrl}}}">' +
        '</a> ' +
        '</div>',
    initialize: function initialize(options) {
        this.naggerImgUrl = chrome.extension.getURL("/img/keep-calm.jpg");
        this.optionsModel = options.optionsModel;
        this.balanceModel = options.balanceModel;
        this.listenTo(this.balanceModel, 'change', this.naggerHandler);
    },
    serializeData: function serializeData() {
        return {serverUrl: this.optionsModel.getServerUrl(),
            naggerImgUrl: this.naggerImgUrl};
    },
    naggerHandler: function naggerHandler() {
        if (this.balanceModel.getBalance() < -8) {
            this.render();
        }
    },
    onRender: function onRender() {
        $.modal(this.el);
        var imgURL = chrome.extension.getURL("/img/x.png");
        $("#simplemodal-container a.modalCloseImg").css('background', "url('" + imgURL + "')  no-repeat");
    }
});