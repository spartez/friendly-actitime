LoggedWorkView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-work-time-template",
    modelEvents: {
        "change": "render"
    },
    initialize: function initialize() {
        this.listenTo(this.options.balanceModel, 'change:today', this.render);
    },
    serializeData: function serializeData() {
        var data = {loggedTime: "00:00", toBeLoggedTime: "00:00"};

        var startTime = this.model.getStartTime();
        var doneTime = this.model.getStopTime(startTime) - startTime;
        if (startTime) {
            data.toBeLoggedTime = TimeUtils.formattedInterval(doneTime);
        }

        var today = this.options.balanceModel.getToday();
        if (today) {
            data.loggedTime = TimeUtils.formattedIntervalHours(today);
        }

        var sumToBeLogged = 0;
        sumToBeLogged += doneTime || 0;
        sumToBeLogged += TimeUtils._hoursToMilis(today) || 0;
        data.sumToBeLogged = TimeUtils.formattedInterval(sumToBeLogged);

        return data;
    }
});