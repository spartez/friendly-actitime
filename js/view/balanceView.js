BalanceView = Backbone.Marionette.ItemView.extend({
    template: "#factitime-balance-template",
    modelEvents: {
        "change": "render"
    },
    serializeData: function serializeData() {
        return {balance: this.model.getBalance(),
            cls: this._getClass() };
    },
    _getClass: function _getClass() {
        if (this.model.getBalance() < 0) {
            return "no-overtime";
        } else {
            return "overtime";
        }
    }
});