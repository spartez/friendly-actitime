class BalanceController extends Backbone.Marionette.Controller {
    initialize({optionsModel, model, isPopup}) {
        this.optionsModel = optionsModel;
        this.model = model;
        // Wait for options to load
        this.listenTo(this.optionsModel, "change", () => this._loadModel(isPopup));
    }

    _loadModel(forceRefresh) {
        let browsingActitimePage = this.optionsModel.getServerUrl() === window.location.origin;
        if (forceRefresh || browsingActitimePage) {
            this.refreshBalance();
        } else {
            this.model.load();
        }
    }

    refreshBalance() {
        let serverUrl = this.optionsModel.getServerUrl();
        let startBalance = this.optionsModel.getStartBalance();
        let startDate = this.optionsModel.getStartDate();
        if (!_.isUndefined(serverUrl)) {
            BalanceLoader.initializeBalanceFromActitimeServer(this.model, serverUrl, startDate, startBalance);
        }
    }
}