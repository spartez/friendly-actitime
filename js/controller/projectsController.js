class ProjectsController extends Backbone.Marionette.Controller {
    initialize({view, model, formModel}) {
        this.view = view;
        this.model = model;
        this.formModel = formModel;
        this.listenTo(this.formModel, "change", this._loadModel);
        this._loadModel();
    }
    _loadModel() {
        let form = this.formModel.getForm();
        if (!_.isUndefined(form)) {
            this.model.initializeProjectsFromActitimeServer(form);
        }
    }
}