class BalanceLoader {
    static initializeBalanceFromActitimeServer(model, serverUrl, startDate, startBalance) {
        let startDateStr = TimeUtils.formattedDate(new Date(startDate));
        let endDateStr = TimeUtils.formattedDate(new Date());
        $.ajax({
            url: serverUrl + "/user/view_tt.do",
            data: {
                'rnd': 1,
                'submitted': true,
                'dateRangeSelector.fromDate.internalText': startDateStr,
                'dateRangeSelector.toDate.internalText': endDateStr,
                'dateRangeSelector.dateRangeState': "range.state.custom",
                'customersProjectsSelector.coarseSelection': "all",
                'showDatesInChronologicalOrder': "on"
            },
            dataType: 'html'
        }).done((data) => {
            let cleanData = data.replace(/<img\s[^>]*?src\s*=\s*['\"]([^'\"]*?)['\"][^>]*?>/g, "");   //todo move to global let
            if (BalanceLoader._isLoggedIn(cleanData)) {
                let workingDaysAndToday = BalanceLoader._getWorkingDaysAndToday(cleanData);
                let workedHours = BalanceLoader._getWorkedHours(workingDaysAndToday.previousDays);
                let workedTodayHours = BalanceLoader._getWorkedHours(workingDaysAndToday.today);
                // We need to subtract overtime as it's counted 2 times (when done and when returned)
                let overtimeDiffHours = BalanceLoader._getOvertime(cleanData);
                let balance = workedHours - workingDaysAndToday.workingDaysNumber * 8 + startBalance - overtimeDiffHours;

                let userName = $('.userProfileLink', cleanData).text().trim();
                model.set({balance: balance, today: workedTodayHours, userName: userName});
                model.save();
            } else {
                model.set({balance: NaN, today: undefined});
                model.save();
            }
        });
    }
    static _getWorkedHours(workingDays) {
        return _.reduce(workingDays, function reduce(memo, row) {
            let hoursString = $(row).text().trim();
            let hours = TimeUtils.parseStringTimeToFloatHours(hoursString);
            return memo + hours;
        }, 0);
    }
    static _getOvertime(data) {
        let overtimeDiff = $('td.leaves-title:contains("Overtime consuming")', data).next().text();
        if (overtimeDiff) {
            return TimeUtils.parseStringTimeToFloatHours(overtimeDiff);
        } else {
            return 0;
        }
    }
    static _isLoggedIn(context) {
        return $('#loginBox', context).length == 0;
    }
}

BalanceLoader._getWorkingDaysAndToday = _.memoize(function (context) {
    // assumption: chronologiczne daty, dzisiejsza data jest ostatnią
    let allWorkingDays = $('table.working-day-header', context);
    let allDays = $('table.working-day-header, table.nonworking-day-header', context);
    let today = $('thead .view-tt-time-column', allDays[allDays.length - 1]);
    allDays.splice(-1, 1);  // remove today
    allWorkingDays.splice(-1, 1);  // remove today
    let isWorkingDay = false;
    let daysSinceFirstDayInWork = _.filter(allDays, function (wd) {
        if (isWorkingDay == false) {
            isWorkingDay = $('.view-tt-time-column', wd).first().text().trim() !== "0:00";
        }
        return isWorkingDay;
    });
    let wdTimeColumns = _.map(daysSinceFirstDayInWork, function (workDay) {
        return $('thead .view-tt-time-column', workDay);
    });
    return {previousDays: wdTimeColumns, workingDaysNumber: allWorkingDays.length, today: today};
});