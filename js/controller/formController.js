class FormController extends Backbone.Marionette.Controller {
    initialize({optionsModel, model}) {
        this.model = model;
        this.optionsModel = optionsModel;
        this.listenTo(this.optionsModel, "change", this._loadModel);
        this._loadModel();
    }
    _loadModel() {
        let serverUrl = this.optionsModel.getServerUrl();
        if (!_.isUndefined(serverUrl)) {
            this.model.initializeFormFromActitimeServer(serverUrl);
        }
    }
}
