class StartTimeController extends Backbone.Marionette.Controller {
    initialize(options) {
        this.workTimeModel = options.workTimeModel;
        this.listenTo(this.workTimeModel, "change", this._loadModel);
        this._loadModel();
    }
    _loadModel() {
        let startTime = this.workTimeModel.getStartTime();
        if(!_.isUndefined(startTime)){
            let isSavedToday = this._isTimeToday(startTime);
            if (!isSavedToday) {
                let now = new Date();
                this.workTimeModel.setStartTime(now);
                this.workTimeModel.save();
            }
        }
    }
    _isTimeToday(savedTime) {
        return new Date(savedTime).toDateString() == new Date().toDateString();
    }
}
