# Friendly Actitime 

## Description
Friendly Actitime aka Factitime is a Chrome extention that allows you to log your work into your hosted Actitime instance through 1 click.

![Factitime popup](https://bitbucket.org/spartez/friendly-actitime/raw/master/img/factitime-demo.png)

## Features list
- reminds you to fill Actitime in case you missed
- remembers what time have you started today's work (based on any page reload)
- adjusts work time to 15 min intervals
- selects recently used project by default
- shows your current work hours balance
- supports working with multiple tasks a day

## Installation/Building
1. Go to chrome://extensions
2. Drag this repo directory onto extensions page
3. or Click "Install extensions without package" and select directory with Friendly Actitime

## Compatibility
- Google Chrome latest version
- Actitime 3.3

## Google Analytics

This plugin uses Google Analytics to monitor its usage. Analytics are enabled by default.
It gathers following events:
- plugin popup shows up
- user successfully logged work time entry + logged hours value
- user changed start time, stop time or project

### How to disable analytics

Comment out following line:

    // analytics/analytics.js
    
    //ga('create', 'UA-50868675-6', 'auto');

## Credits
- [Damian Skrodzki](https://bitbucket.org/damianskrodzki)
- [Michał Wyrzykowski](https://bitbucket.org/michal_wyrzykowski)

